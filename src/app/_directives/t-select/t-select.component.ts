import { Component, OnInit, ContentChild, ElementRef, Renderer2, Input } from '@angular/core';

@Component({
  selector: 't-select',
  templateUrl: './t-select.component.html',
  styleUrls: ['./t-select.component.scss']
})
export class TSelectComponent implements OnInit {
  @ContentChild('title') title_elem: ElementRef;
  @Input('default') default: any;
  parent: HTMLElement;
  select: HTMLSelectElement;

  constructor(
    private renderer: Renderer2,
    private elRef: ElementRef
  ) {
    this.parent = this.elRef.nativeElement;
  }

  ngOnInit() {
    if (this.title_elem) {
      this.renderer.addClass(this.title_elem.nativeElement , 'title');
    } else {
      this.parent = this.elRef.nativeElement;
    }
    this.select = <HTMLSelectElement>this.parent.getElementsByTagName('select')[0];
    if (!this.default) {
      setTimeout(()=>{
        this.default = this.select.value;
      })

    }
    this.select.addEventListener('change', e => this.toggle_class(e, this.parent));
  }

  toggle_class(e, elem) {
    (this.check_value(e)) ? this.renderer.removeClass(elem, 'selected') : this.renderer.addClass(elem, 'selected') ;
  }

  check_value = e => e.target.value === this.default;
}
