import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TCheckboxComponent } from './t-checkbox.component';

describe('TCheckboxComponent', () => {
  let component: TCheckboxComponent;
  let fixture: ComponentFixture<TCheckboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TCheckboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TCheckboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
