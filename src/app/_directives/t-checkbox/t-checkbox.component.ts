import { Component, OnInit, ContentChild, ElementRef, Renderer2 } from '@angular/core';

@Component({
  selector: 't-checkbox',
  templateUrl: './t-checkbox.component.html',
  styleUrls: ['./t-checkbox.component.scss']
})
export class TCheckboxComponent implements OnInit {
  @ContentChild('title') title: ElementRef;
  parent: HTMLElement;
  checkbox: HTMLInputElement;

  constructor(
    private renderer: Renderer2,
    private elRef: ElementRef
  ) { }

  ngOnInit() {
    this.renderer.addClass(this.title.nativeElement, 'title');
    this.parent = this.elRef.nativeElement;
    this.checkbox = <HTMLInputElement>this.parent.getElementsByTagName('input')[0];
    this.checkbox.addEventListener('change', e => this.toggle_class(e, this.parent))
  }

  toggle_class(e, elem) {
    e.target.checked ? this.renderer.addClass(elem, 'selected') : this.renderer.removeClass(elem, 'selected') ;
  }
}
