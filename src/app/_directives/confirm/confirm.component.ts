import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-confirm',
  templateUrl: './confirm.component.html',
  styleUrls: ['./confirm.component.scss']
})

export class ConfirmComponent implements OnInit {
  private error = '';
  private success = '';

  constructor(
    public bsModalRef: BsModalRef
  ) { }

  ngOnInit() {
  }

  private ok() {
    this.bsModalRef.hide();
  }
}
