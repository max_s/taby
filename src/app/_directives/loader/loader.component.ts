/*
*
*     Usage:
*     <app-loader></app-loader> -- Global
*     <app-loader [relation]='true' color='#fff' ></app-loader> -- Local
*
*/

import { Component, OnInit, Renderer2, OnDestroy, Input, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription'

import { LoaderService } from '@app/_services';

@Component({
  selector: 'app-loader',
  templateUrl: './loader.component.html',
  styleUrls: ['./loader.component.scss']
})

export class LoaderComponent implements OnInit, OnDestroy {
  @Input() relation: boolean = false;
  @Input() color: boolean | string = false;
  _body: HTMLElement = document.body;
  show = false ;
  subscription: Subscription;
  elem: HTMLElement;
  constructor(
    private renderer: Renderer2,
    private elRef: ElementRef,
    private loaderService: LoaderService
  ) { }

  ngOnInit() {
    if (this.relation) {
      this.elem = this.elRef.nativeElement.parentElement;
      this.show = true;
      this.renderer.addClass(this.elem, 'relative');
    }
    this.subscription = this.loaderService.getLoader().subscribe(
      e => {
        if (e.status && !this.relation) {
          this.on_show()
        } else {
          this.on_hide();
        }
      }
    )
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.on_hide();
  }

  on_show(){
    this.renderer.addClass(this._body, 'open_modal');
    this.show = true;
  }

  on_hide(){
    if (this.elem && this.elem.classList.contains('relative')) {
      this.renderer.removeClass(this.elem, 'relative');
    }
    this.renderer.removeClass(this._body, 'open_modal');
    this.show = false;
  }
}
