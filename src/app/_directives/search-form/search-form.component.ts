import { Component, OnInit, Input } from '@angular/core';
import { SearchService } from '@app/_services';
@Component({
  selector: 'app-search-form',
  templateUrl: './search-form.component.html',
  styleUrls: ['./search-form.component.scss']
})
export class SearchFormComponent implements OnInit {
  constructor(
    private searchService: SearchService
  ) { }

  ngOnInit() {
  }

  show_adv_search() {
    this.searchService.toogle_search();
  }
}
