import {Component, Directive, ElementRef, OnInit, QueryList, Renderer2, ViewChildren, Input, forwardRef, AfterViewInit} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

export const VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => PropertyTableRowComponent),
  multi: true
};

@Directive({
  selector: '[paw]'
})

export class PawDirective {
  elem: HTMLElement;

  constructor(
    private elRef: ElementRef,
    private renderer: Renderer2,
  ){
    this.elem = elRef.nativeElement;
  }

  toggle_selected(action: boolean) {
    action ? this.renderer.addClass(this.elem, 'selected') : this.renderer.removeClass(this.elem, 'selected');
  }
}


@Component({
  selector: 'app-property-table-row',
  templateUrl: './property-table-row.component.html',
  styleUrls: ['./property-table-row.component.scss'],
  providers: [ VALUE_ACCESSOR ]
})

export class PropertyTableRowComponent implements OnInit, ControlValueAccessor, AfterViewInit {
  @ViewChildren(PawDirective) paws: QueryList<PawDirective>;
  @Input() prop: any = {};
  @Input() stars: number;
  @Input() disable_state: boolean = true;
  _value: number = 0;
  row: HTMLElement;
  points = [];
  private change_value: Function;

  constructor(
    private elRef: ElementRef
  ) {

  }

  ngAfterViewInit() {
    this.row = this.elRef.nativeElement.querySelector('.search_option_attr');
  }

  ngOnInit() {
    this._value = +this.prop.val;
    this.set_points(+this.prop.val);
  }

  registerOnChange(fn: any) {
    this.change_value = fn;
  }
  writeValue(value: any) {}

  registerOnTouched(fn: any) {}

  set_points(value: number) {
    for(let i = 0, x = this.stars; i < x; i++ ) {
      this.points.push({state: i < value});
    }
  }

  toggle_row_color(action: boolean, row: HTMLElement) {
    action ? row.classList.add('selected') : row.classList.remove('selected');
  }

  pick(i: number) {
    if (this.value === i+1) {
      this.value = 0;
      this.toggle_row_color(false, this.row);
      return this.paws.toArray().forEach( el => el.toggle_selected(false));
    }
    this.toggle_row_color(true, this.row);
    this.value = i+1;
    this.paws.toArray().forEach( (el, index) => el.toggle_selected(index <= i));
  }

  get value() {
    return this._value;
  }

  set value(v: number) {
    this._value = v;
    this.change_value(v);
  }
}
