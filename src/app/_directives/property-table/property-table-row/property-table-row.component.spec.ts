import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProppertyTableRowComponent } from './property-table-row.component';

describe('ProppertyTableRowComponent', () => {
  let component: ProppertyTableRowComponent;
  let fixture: ComponentFixture<ProppertyTableRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProppertyTableRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProppertyTableRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
