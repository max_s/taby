import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-property-table',
  templateUrl: './property-table.component.html',
  styleUrls: ['./property-table.component.scss']
})
export class PropertyTableComponent implements OnInit {
  @Input() properties: any;
  @Input() model: any;
  @Input() stars: number = 5;
  @Input() disable_panel: boolean = true;
  constructor() { }

  ngOnInit() {
  }

}
