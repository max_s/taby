import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OAuthLinkComponent } from './oauth-links.component';

describe('OAuthLinkComponent', () => {
  let component: OAuthLinkComponent;
  let fixture: ComponentFixture<OAuthLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OAuthLinkComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OAuthLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
