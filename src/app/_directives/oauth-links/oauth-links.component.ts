import { Component, OnInit } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { AuthService } from '@app/_services/';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { RegistrationComponent } from '@app/components/registration/registration.component';
import { ConfirmComponent } from '@app/_directives';
import { PhoneConfirmComponent } from '@app/components/phoneconfirm/phoneconfirm.component';
import { RestorePasswordComponent } from '@app/components/restorepassword/restorepassword.component';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { environment } from '@env/environment';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-oauth-links',
  templateUrl: './oauth-links.component.html',
  styleUrls: ['./oauth-links.component.scss']
})
export class OAuthLinkComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private localStorageService: LocalStorageService,
    private router: Router,
    private modalService: BsModalService,
    private bsModalRef: BsModalRef
  ) { }

  ngOnInit() {
  }

  private _redirect_uri = `redirect_uri=${environment.host_url}/oauth/`;
  private authenticatedObs: Observable<boolean>;
  private userServiceSub: Subscription;
  private authSub: Subscription;

  private login_fb() {
    let scope = 'scope=email';
    let response_type = 'response_type=code';
    let redirect_uri = `${this._redirect_uri}fb`;
    let url = `https://www.facebook.com/v3.0/dialog/oauth?client_id=${environment.client_id_fb}&${redirect_uri}&provider=fb&${scope}&${response_type}`;
    this.openAuthWindow(url);
  }

  private login_in() {
    let scope = 'scope=basic+public_content';
    let response_type = 'response_type=code';
    let redirect_uri = `${this._redirect_uri}in`;
    let url = `https://api.instagram.com/oauth/authorize/?client_id=${environment.client_id_in}&${redirect_uri}&provider=in&${scope}&${response_type}`;
    this.openAuthWindow(url);
  }

  private login_vk() {
    let scope = 'scope=4194304';
    let display = 'display=page';
    let response_type = 'response_type=code';
    let redirect_uri = `${this._redirect_uri}vk`;
    let url = `https://oauth.vk.com/authorize?client_id=${environment.client_id_vk}&${redirect_uri}&provider=vk&${scope}&${display}&${response_type}&v=5.74`;
    this.openAuthWindow(url);
  }

  private login_ok() {
    let scope = 'scope=GET_EMAIL';
    let display = 'layout=w';
    let response_type = 'response_type=code';
    let redirect_uri = `${this._redirect_uri}ok`;
    let url = `https://connect.ok.ru/oauth/authorize?client_id=${environment.client_id_ok}&${redirect_uri}&provider=ok&${scope}&${display}&${response_type}`;
    this.openAuthWindow(url);
  }

  private openAuthWindow(url: string) {
    var newWindow = window.open(url, 'name', 'height=585, width=770');
    if (window.focus) {
      newWindow.focus();
    }

    let source = Observable.interval(2000)
      .map(() => {
        this.userServiceSub = this.authenticated().subscribe(data => {
          if (data) {
            newWindow.close();
            this.bsModalRef.hide();
          }
        })
      })

    if (this.authSub) this.authSub.unsubscribe();
    this.authSub = source.subscribe();
  }

  private authenticated(): Observable<boolean> {
    this.authenticatedObs = new Observable<boolean>((observer) => {
      observer.next(this.authService.signedIn());
    });

    return this.authenticatedObs;
  }
}
