import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, LoaderService } from '@app/_services';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { LocalStorageService } from 'angular-2-local-storage';
import { LoginComponent } from '@app/components/login/login.component';

@Component({
  selector: 'nav-menu',
  templateUrl: './navmenu.component.html'
})

export class NavMenuComponent implements OnInit {
  is_open = false;
  constructor(
    private router: Router,
    private authService: AuthService,
    private modalService: BsModalService,
    private loader: LoaderService,
    private localStorageService: LocalStorageService
  ) { }

  ngOnInit() {
    window.addEventListener('click', (e: Event) => {
      let target: HTMLElement = <HTMLElement>e.target;
      if (!target.closest('.header__menu')) {
        if (document.querySelector('.menu_open')) {
          this.is_open = false;
        }
      }
    })
  }

  open_menu = () => this.is_open = !this.is_open;

  signIn() {
    this.is_open = false;
    this.modalService.show(LoginComponent);
  }

  signOut() {
    this.loader.show();
    this.authService.signOut()
      .then((data) => {
        this.localStorageService.clearAll();
        this.is_open = false;
        this.loader.hide();
        this.router.navigate(['/']);
      }).catch((error) => {
        console.error(error);
        this.loader.hide();
      });
  }
}
