import { Component, OnInit, Input } from '@angular/core';
import { ConstructorService } from '@app/_services';

import { forkJoin } from 'rxjs/observable/forkJoin';

type PICKED_PROPS = 'white' | 'shaded' | 'silver' | 'point' | 'tabby' | 'tortie' | 'smoke';



@Component({
  selector: 'app-constructor-properties',
  templateUrl: './constructor-properties.component.html',
  styleUrls: ['./constructor-properties.component.scss']
})
export class ConstructorPropertiesComponent implements OnInit {
  @Input() model: any;
  color: any[];
  default_state: any;
  pattern: any;
  breed: any;
  point_color: any;
  eyes: any;
  odd_eye: any;
  whitespot: any;
  silver_gold_color: any;
  picked = {
    white: false,
    shaded: false,
    silver: false,
    point: false,
    tabby: false,
    tortie: false,
    smoke: false
  };


  constructor(
    private constructorService: ConstructorService
  ) { }

  ngOnInit() {
    forkJoin(
      this.constructorService.get_main_color(),
      this.constructorService.get_eyes(),
      this.constructorService.get_tabby_patterns(),
      this.constructorService.get_silver(),
      this.constructorService.get_point_color(),
      this.constructorService.get_whitespot(),
      this.constructorService.get_odd_eyes(),
      this.constructorService.get_breed(),
    ).subscribe( data => {
      this.set_responce('color', data[0]);
      this.set_responce('eyes', data[1], 'svg');
      this.set_responce('pattern', data[2]);
      this.set_responce('silver_gold_color', data[3], 'value');
      this.set_responce('point_color', data[4]);
      this.set_responce('whitespot', data[5]);
      this.set_responce('odd_eye', data[6]);
      this.set_responce('breed', data[7]);
      this.default_state = {...this.model};
      delete  this.default_state.color;
      delete  this.default_state.eyes;
    })
  }

  pick_params(e: Event, input_type?: string){
    this.set_picked('white', this.model.color.color_let.toLowerCase() === 'w' ? true : false);
    this.set_picked('shaded', /shaded|shell/.test(this.model.pattern.svg));
    this.set_picked('silver', this.model.silver_gold_color ? true : false);
    this.set_picked('smoke', this.model.smoke);
    this.set_picked('point', this.model.point_color.svg ? true : false);
    this.set_picked('tabby', /tabby/.test(this.model.pattern.svg));
    this.set_picked('tortie', this.check_tortie(this.model.color.color_let));

    this.set_tortie = this.check_tortie(this.model.color.color_let);

    this.model.gradient = /shaded|shell|tabby_7/.test(this.model.pattern.svg);


    if (this.get_picked.shaded){
      this.model.silver_gold_color = this.get_prop('silver_gold_color', 'silver', 'title_en').value;
    }



    if (this.get_picked.white) {
      this.reset_state('all');
    }

    if (input_type === 'smoke') {
      if (!this.get_picked.tabby && !this.get_picked.silver && e) {
        this.model.silver_gold_color = this.get_prop('silver_gold_color', 'silver', 'title_en').value;
        this.set_picked('silver', true);
      } else {
        this.model.silver_gold_color = this.get_prop('silver_gold_color', null, 'value').value;
        this.set_picked('silver', false);
      }
    }

    // if (this.get_picked.smoke && !this.get_picked.silver) {
    //   this.model.silver_gold_color = this.get_prop('silver_gold_color', 'silver', 'title_en').value;
    //   this.set_picked('silver', true);
    // }

    if (this.get_picked.silver) this.model.eyes = 'eyes_5';


    if (!this.get_picked.tabby && this.get_picked.silver && input_type !== 'smoke') {
      this.model.smoke = true;
    }





    if (this.get_picked.tabby || this.get_picked.shaded) this.model.smoke = false;



    // if (!this.get_picked.tabby && !this.get_picked.silver && this.get_picked.smoke){
    //   this.set_picked('silver', true);
    //   this.model.silver_gold_color = this.get_prop('silver_gold_color', 'silver', 'title_en').value;
    // }

    if (this.get_picked.point) {
      this.model.eyes = this.model.point_color.eyes_svg;
      if (this.get_picked.shaded) {
        return false;
      }
      if (this.model.pattern.svg && !/tabby_3/.test(this.model.pattern.svg)) {
        this.model.pattern = this.get_prop('pattern', 'tabby_3');
      }
      // this.set_tortie = false;
      // this.model.pattern = this.pattern[0];
    }

  }

  check_tortie = color_let => ['f','h','q','g','j','r'].some( r =>  color_let === r );

  set set_tortie (val) {
    this.model.has_tortie = val;
  }

  // getter setter

  set_picked = (property: PICKED_PROPS, value: boolean) => this.picked[property.trim()] = value;

  get get_picked () {
    return this.picked
  };

  set_responce(property: string, value: Object, mod?: string) {
    this[property] = value;
    this.model[property] = mod ? value[0][mod] : value[0];
  }

  reset_state(param: string) {
    if (param === 'all') {
      this.model =  Object.assign(this.model, this.default_state);
      return;
    }
    this.model[param] =  this.default_state[param];
  }

  get_prop = (prop: string, param: string, path: string = 'svg') => this[prop].filter(el => `${el[path]}`.toLowerCase() === `${param}`.toLowerCase())[0];
}
