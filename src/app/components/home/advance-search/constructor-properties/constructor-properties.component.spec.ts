import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConstructorPropertiesComponent } from './constructor-properties.component';

describe('ConstructorPropertiesComponent', () => {
  let component: ConstructorPropertiesComponent;
  let fixture: ComponentFixture<ConstructorPropertiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ConstructorPropertiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ConstructorPropertiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
