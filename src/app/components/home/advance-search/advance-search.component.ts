import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { SearchService, BreedService, FenotypeService,  } from '@app/_services';
import { BreedData, TraitData, SearchRequestData, SearchTraitData, MainColorData, ColorEyeData, ColorModeData, ColorPointedPatternData, ColorTabbyPatternData, ColorWhiteAmmountData} from '@app/models/_index';


@Component({
  selector: 'app-advance-search',
  templateUrl: './advance-search.component.html',
  styleUrls: ['./advance-search.component.scss']
})
export class AdvanceSearchComponent implements OnInit, OnDestroy {
  private subs:Subscription;
  show_search = false;
  // model: any = {
  //   traits: {},
  //   constructor: {
  //     smoke: false,
  //     gradient: false,
  //     breed : {
  //       "svg": "default"
  //     }
  //   }
  // };

  searchRequest: SearchRequestData = new SearchRequestData();

  breeds : Array<BreedData> = [];
  traits : Array<TraitData> = [];
  mainColors: Array<MainColorData> = [];
  eyeColors: Array<ColorEyeData>  = [];
  colorModes: Array<ColorModeData> = [];
  colorPointedPatterns: Array<ColorPointedPatternData> = [];
  colorTabbyPatterns: Array<ColorTabbyPatternData> = [];
  colorWhiteAmmounts: Array<ColorWhiteAmmountData> = [];

  PROPERTY = [{ //fixme HARDCODE
    'title': 'Adaptable',
    val: 0
  },{
    'title': 'Affectionate',
    val: 0
  },{
    'title': 'Child friendly',
    val: 0
  },{
    'title': 'Dog friendly',
    val: 0
  },{
    'title': 'Stranger friendly',
    val: 0
  },{
    'title': 'Intelligent',
    val: 0
  },{
    'title': 'Social needs',
    val: 0
  },{
    'title': 'Energic',
    val: 0
  }];

  constructor(
    private searchService: SearchService,
    private breedsService: BreedService,
    private fenotypeService: FenotypeService
  ) { }

  ngOnInit() {
    this.subs = this.searchService.advance_search.subscribe(
      (state: boolean) => {
        this.show_search = state;
      }
    )

    this.breedsService.getBreeds().then((data)=>{
      this.breeds = JSON.parse(data._body) as BreedData[];
      
      let allBreedsItem = new BreedData()

      allBreedsItem.id = null;
      allBreedsItem.name = 'All';
      
      this.breeds.unshift(allBreedsItem); 
      
      this.searchRequest.breedId = null;
                      
    });

    this.fenotypeService.getMainColors().then((data)=>{
      let anyColor = new MainColorData();

      anyColor.id = null;
      anyColor.name = 'All';

      this.mainColors = JSON.parse(data._body) as MainColorData[];

      this.mainColors.unshift(anyColor);
      this.searchRequest.colorMainId = null;
    });

    this.fenotypeService.getColorModes().then((data)=>{
      let anyColorMode = new ColorModeData();

      anyColorMode.id = null;
      anyColorMode.name = 'All';

      this.mainColors = JSON.parse(data._body) as ColorModeData[];

      this.mainColors.unshift(anyColorMode);

     
    });

    this.fenotypeService.getColorPointedPatterns().then((data)=>{
      let anyColorPointedPattern = new ColorModeData();

      anyColorPointedPattern.id = null;
      anyColorPointedPattern.name = 'All';

      this.colorPointedPatterns = JSON.parse(data._body) as ColorPointedPatternData[];

      this.colorPointedPatterns.unshift(anyColorPointedPattern);

      
    });

    this.fenotypeService.getColorTabbyPatterns().then((data)=>{
      let anyColorTabbyPattern = new ColorTabbyPatternData();

      anyColorTabbyPattern.id = null;
      anyColorTabbyPattern.name = 'All';

      this.colorTabbyPatterns = JSON.parse(data._body) as ColorTabbyPatternData[];

      this.colorTabbyPatterns.unshift(anyColorTabbyPattern);

      this.searchRequest.colorTabbyPattern = null;
    });

    this.fenotypeService.getEyeColors().then((data)=>{
      let anyEyeColor = new ColorEyeData();

      anyEyeColor.id = null;
      anyEyeColor.name = 'All';

      this.eyeColors = JSON.parse(data._body) as ColorEyeData[];

      this.eyeColors.unshift(anyEyeColor);

      this.searchRequest.colorEyeId = null;
    });

    this.fenotypeService.getColorWhiteAmmounts().then((data)=>{
      let anyColorWhiteAmmount = new ColorWhiteAmmountData();

      anyColorWhiteAmmount.id = null;
      anyColorWhiteAmmount.name = 'All';

      this.colorWhiteAmmounts = JSON.parse(data._body) as ColorWhiteAmmountData[];

      this.colorWhiteAmmounts.unshift(anyColorWhiteAmmount);

      
    });

    this.breedsService.getTraits().then((data)=>{
        this.traits = JSON.parse(data._body) as TraitData[];
    });
  }

  ngOnDestroy(){
    this.subs.unsubscribe();
  }

  close_panel() {
    this.searchService.toogle_search();
  }
}
