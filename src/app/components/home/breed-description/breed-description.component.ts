import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CatalogData, BreedData, TraitData, BreedTraitData } from '@app/models/_index';
import { BreedService, LoaderService } from '@app/_services';

@Component({
  selector: 'app-breed-description',
  templateUrl: './breed-description.component.html',
  styleUrls: ['./breed-description.component.scss']
})
export class BreedDescriptionComponent implements OnInit {
  @Output() close_breed_panel = new EventEmitter();
  @Input() model: any;

  private breed: BreedData = new BreedData();
  private traits: TraitData[];
  private breedTraits: BreedTraitData[];

  PROPERTY = [{ title: '', val: 0 }];

  constructor(
    private breedService: BreedService,
    private loaderService: LoaderService
  ) {
    this.breed.fennologicalSystemNames = [];
  }

  ngOnInit() {
    let catalogData = this.model as CatalogData;

    this.breedService.getBreedsById(catalogData.id).then((data) => {
      this.breed = JSON.parse(data._body) as BreedData;
    });

    this.breedService.getTraits().then((traits) => {
      this.traits = JSON.parse(traits._body) as TraitData[];

      this.breedService.getBreedTraits(catalogData.id).then((data) => {
        this.breedTraits = JSON.parse(data._body) as BreedTraitData[];
        if (this.breedTraits.length == 0) {
          this.PROPERTY = this.traits.map(t => { return { title: t.name, val: 0 } });
        }
        else {
          this.PROPERTY = this.traits.map(t => {
            let trait = this.breedTraits.find(x => x.trait.id === t.id);
            return {
              title: t.name,
              val: trait ? trait.value : 0,
            }
          });
        }
        this.loaderService.hide();
      });
    });
  }

  close() {
    this.close_breed_panel.emit();
  }
}
