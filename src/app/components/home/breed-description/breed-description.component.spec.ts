import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreedDescriptionComponent } from './breed-description.component';

describe('BreedDescriptionComponent', () => {
  let component: BreedDescriptionComponent;
  let fixture: ComponentFixture<BreedDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreedDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreedDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
