import { Component, OnInit, Inject } from '@angular/core';
import { Masonry, MasonryGridItem } from 'ng-masonry-grid';
import { PetService, ErrorHelper} from '@app/_services/';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import {PetsCardComponent} from '@app/components/petscard/petscard.component';
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { environment } from '@env/environment';


@Component({
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.scss']
})


export class HomeComponent implements OnInit {
   PROPERTY = [{
      title: 'Adopt',
      val: 2
    },{
      title: 'Some',
      val: 1
    },{
      title: 'Test prop',
      val: 5
    }];

    private readonly pageSize = 100;
    private page = 0;

    masonry: Masonry;
    galleryItems: Array<any> = [];
    count = 0;
    show_panel = false;
    show_advance_search = false;
    picked_breed_model = {
      title: 'Breed 1'
    };

    constructor(
      private petService: PetService,
      private errorHelper: ErrorHelper,
      private modalService: BsModalService,) {

        const len = 10; // length of grid items
        for (let i = 0; i < len; i++) {
          //this.galleryItems.push({ src: this.getSrc(), count: this.count++});
        }
      }

      getRandomInt(min: number, max: number) {
        return  Math.floor( Math.random() * max + min );
      }
      getSrc() {
        // const width = this.getRandomInt( 300, 400 );
        // const height = this.getRandomInt( 300, 500 );
        // return 'http://lorempixel.com/'  + width + '/' + height + '/nature';
        return '../assets/images/msn/' + this.getRandomInt(1, 15) + '.png';
      }

      ngOnInit(): void {
        this.petService.getPetsForLanding(this.page, this.pageSize).then((data)=>{

          let items = JSON.parse(data._body);

          for(var i = 0; i < items.length; i++){
             let item = items[i];

             item.src = this.getSrc();

             if(item.pet && item.pet.petProfileImagePath){
               item.pet.petProfileImagePath = environment.apiHost + item.pet.petProfileImagePath;
             }

             this.galleryItems.push(item);
           }
        });
      }

      showPetsCard(petId: number){

        const initialState = { petId: petId };

        this.modalService.show(PetsCardComponent, {initialState});
      }

      toggle_panel = () => this.show_panel = !this.show_panel;

      show_breed_panel(data: any) {
        this.toggle_panel();
        this.picked_breed_model = data;
      }
      toogle_advance_search() {
        //this.show_advance_search = !this.show_advance_search;
      }

      onScroll(){
        if(this.galleryItems.length!=0){
          this.page++;

          this.ngOnInit();
        }
      }
}
