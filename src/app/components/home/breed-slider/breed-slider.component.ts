import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BreedService } from '@app/_services';
import { CatalogData } from '@app/models/_index';
declare const $: any;
declare const Swiper: any;

@Component({
  selector: 'app-breed-slider',
  templateUrl: './breed-slider.component.html',
  styleUrls: ['./breed-slider.component.scss', './swiper.scss']
})

export class BreedSliderComponent implements OnInit {
  @Output() picked_breed = new EventEmitter();
  private breedList: CatalogData;

  constructor(
    private breedService: BreedService
  ) { }

  ngOnInit() {
    this.breedService.getBreeds().then((data) => {
      this.breedList = JSON.parse(data._body);
      setTimeout(() => this.init_slider());
    });
  }

  pick = (data: any) => this.picked_breed.emit(data);

  init_slider() {
    new Swiper('.swiper-container', {
      nextButton: '.category__arr--right',
      prevButton: '.category__arr--left',
      slidesPerView: 12,
      centeredSlides: true,
      loop: true,
      breakpoints: {
        320: {
          slidesPerView: 3,
          spaceBetween: 10
        },
        480: {
          slidesPerView: 4,
          spaceBetween: 20
        },
        640: {
          slidesPerView: 6,
          spaceBetween: 30
        },
        768: {
          slidesPerView: 7,
          spaceBetween: 30
        },
        1054: {
          slidesPerView: 9,
          spaceBetween: 30
        }
      }
    })

  }

}
