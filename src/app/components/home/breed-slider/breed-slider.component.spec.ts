import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreedSliderComponent } from './breed-slider.component';

describe('BreedSliderComponent', () => {
  let component: BreedSliderComponent;
  let fixture: ComponentFixture<BreedSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreedSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreedSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
