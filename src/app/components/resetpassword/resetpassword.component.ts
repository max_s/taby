import { Component, OnInit } from '@angular/core';
import { LoginData, AuthData, JwtData, ConfirmData, ResetPasswordData } from '../../models/_index';
import { AuthService, ErrorHelper, LoaderService, RegistrationService } from '@app/_services/';
import { LocalStorageService } from 'angular-2-local-storage';
import { Router } from '@angular/router';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { RegistrationComponent } from '@app/components/registration/registration.component';
import { ConfirmComponent } from '@app/_directives';

@Component({
    selector: 'app-resetpassword',
    templateUrl: './resetpassword.component.html',
    styleUrls: ['./resetpassword.component.scss']
})
export class ResetPasswordComponent implements OnInit {

    private userName: string;
    private token: string;
    private resetModel: ResetPasswordData;
    private error = '';
    private isPhone = false;

    constructor(
        private router: Router,
        private errorHelper: ErrorHelper,
        private modalService: BsModalService,
        private loader: LoaderService,
        private registrationService: RegistrationService,
        public bsModalRef: BsModalRef
    ) { }

    ngOnInit() {
        this.resetModel = new ResetPasswordData()
        this.resetModel.userName = this.userName;
        this.resetModel.resetToken = this.token;
    }

    private reset() {
        if (this.resetModel.passwordConfirmation != this.resetModel.password) {
            this.error = 'Password and Confirm password is not match';
            return;
        }

        this.loader.show();
        this.error = '';

        this.registrationService.resetPassword(this.resetModel)
            .then((data) => {
                this.loader.hide();
                this.bsModalRef.hide();
                const initialState = { success: 'Password was changed. Please use login form.' };
                this.modalService.show(ConfirmComponent, { initialState });
            }).catch((error) => {
                this.error = this.errorHelper.parseError(error);
                this.loader.hide();
            });
    }
}
