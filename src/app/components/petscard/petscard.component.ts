import { Component, OnInit } from '@angular/core';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { PetService, ErrorHelper, LoaderService} from '@app/_services/';
import { PetCardData } from '@app/models/_index';
import { environment } from '@env/environment';

@Component({
    templateUrl: 'petscard.component.html',
    styleUrls: ['./petscard.component.scss']
})


export class PetsCardComponent implements OnInit {

    private petId: number;

    petCardData: PetCardData;


    constructor(
      private modalService: BsModalService,
      private petService: PetService,
      private loaderService: LoaderService
    ) {}

      ngOnInit(): void {
        this.loaderService.show();
        this.petService.getPet(this.petId).then((data)=>{
            let pet = JSON.parse(data._body);

            this.petCardData = new PetCardData();

            this.petCardData.id = pet.id;
            this.petCardData.name = pet.name;
            this.petCardData.age = pet.ageStringified;
            this.petCardData.breed = pet.breed?pet.breed.name:null;
            this.petCardData.cattery = pet.cattery?pet.cattery.name:null;
            this.petCardData.price = 1000;
            this.petCardData.description = pet.description;
            this.petCardData.sex = pet.sex == 1?"male":"female";
            this.petCardData.colorCode = pet.fenotype?pet.fenotype.code:null;
            this.petCardData.color = '';
            this.petCardData.father = new PetCardData();
            this.petCardData.mother = new PetCardData();

            if(pet.petProfileImagePath){
                this.petCardData.profileImageUrl = environment.apiHost + pet.petProfileImagePath;
            }

            if(pet.fenotype){
                if(pet.fenotype.colorMain){
                    this.petCardData.color+=pet.fenotype.colorMain.name+' ';
                }

                if(pet.fenotype.colorMode){
                    this.petCardData.color+=pet.fenotype.colorMode.name+' ';
                }

                if(pet.fenotype.colorDeluteModifier){
                    this.petCardData.color+=pet.fenotype.colorDeluteModifier.name+' ';
                }

                if(pet.fenotype.colorEye){
                    this.petCardData.color+=pet.fenotype.colorEye.name+' ';
                }

                if(pet.fenotype.colorPointedPattern){
                    this.petCardData.color+=pet.fenotype.colorPointedPattern.name+' ';
                }

                if(pet.fenotype.colorTabbyPattern){
                    this.petCardData.color+=pet.fenotype.colorTabbyPattern.name+' ';
                }

                if(pet.fenotype.colorWhiteAmount){
                    this.petCardData.color+=pet.fenotype.colorWhiteAmount.name+' ';
                }

            }

            if(pet.father){

                this.petCardData.father.name = pet.father.name;
                this.petCardData.father.colorCode = pet.father.fenotype?pet.father.fenotype.code:null;

                if(pet.father.petProfileImagePath){
                    this.petCardData.father.profileImageUrl = environment.apiHost +  pet.father.petProfileImagePath;
                }
            }

            if(pet.mother){

                this.petCardData.mother.name = pet.mother.name;
                this.petCardData.mother.colorCode = pet.mother.fenotype?pet.mother.fenotype.code:null;

                if(pet.mother.petProfileImagePath){
                    this.petCardData.mother.profileImageUrl = environment.apiHost +  pet.mother.petProfileImagePath;
                }
            }
          this.loaderService.hide();
        }).catch(err => console.error("ERROR: ", err))
      }

      showPetsCard(petId: number){

      }
}
