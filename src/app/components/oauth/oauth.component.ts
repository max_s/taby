import { Component, OnInit, Inject } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { AuthService, ErrorHelper, LoaderService } from '@app/_services';
import { AuthData, JwtData } from '@app/models/_index';
import { LocalStorageService } from 'angular-2-local-storage';
import { environment } from '@env/environment';

@Component({
    templateUrl: 'oauth.component.html'
})

export class OauthComponent implements OnInit {
    status: string;

    constructor(
        private authService: AuthService,
        private activatedRoute: ActivatedRoute,
        private localStorageService: LocalStorageService,
        private errorHelper: ErrorHelper,
        private loader: LoaderService,
        private router: Router) { }

    ngOnInit() {
        this.loader.show();

        this.activatedRoute.queryParams.subscribe((params: Params) => {
            let provider = this.activatedRoute.snapshot.url[1].toString();
            let code = params['code'];
            let token = params['t'];
            let redirect_url = `${environment.host_url}/oauth/${provider}`;

            this.authService.signIn_oauth(provider, code, redirect_url, token)
                .then((data) => {
                    let authData = JSON.parse(data._body) as AuthData;
                    let jwtData = JSON.parse(authData.data) as JwtData;
                    this.localStorageService.set('currentUser', jwtData);
                    this.status = 'success';
                }).catch((error) => {
                    this.status = this.errorHelper.parseError(error);
                });
        });
    }
}
