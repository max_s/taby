import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailCallBackComponent } from '@app/components/emailcallback/emailcallback.component';

describe('EmailCallBackComponent', () => {
  let component: EmailCallBackComponent;
  let fixture: ComponentFixture<EmailCallBackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [EmailCallBackComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailCallBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
