import { Component, OnInit } from '@angular/core';
import { LoginData, AuthData, RegistrationData, ConfirmData } from '../../models/_index';
import { AuthService, ErrorHelper, RegistrationService, LoaderService } from '@app/_services/';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
import { ConfirmComponent } from '@app/_directives';
import { ResetPasswordComponent } from '@app/components/resetpassword/resetpassword.component';


@Component({
  selector: 'app-emailcallback',
  templateUrl: './emailcallback.component.html',
  styleUrls: ['./emailcallback.component.scss']
})
export class EmailCallBackComponent implements OnInit {

  constructor(
    private errorHelper: ErrorHelper,
    private loader: LoaderService,
    private activatedRoute: ActivatedRoute,
    private registrationService: RegistrationService,
    private modalService: BsModalService
  ) {
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((params: Params) => {
      let userName = params['userName'];
      let token = params['token'];
      let type = params['t'];
      if (type && userName && token && type == 1) {
        this.loader.show();
        let confirmModel = new ConfirmData();
        confirmModel.username = userName;
        confirmModel.token = token;
        this.registrationService.confirmEmail(confirmModel)
          .then((data) => {
            this.loader.hide();
            const initialState = { success: 'Email is confirmed' };
            this.modalService.show(ConfirmComponent, { initialState });
          }).catch((error) => {
            let errorMsg = this.errorHelper.parseError(error);
            const initialState = { error: errorMsg };
            this.modalService.show(ConfirmComponent, { initialState });
            this.loader.hide();
          });
      }
      if (type && userName && token && type == 2) {
        const initialState = { userName: userName, token: token };
        this.modalService.show(ResetPasswordComponent, { initialState });
      }
    });
  }
}
