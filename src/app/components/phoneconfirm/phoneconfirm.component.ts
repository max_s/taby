import { Component, OnInit } from '@angular/core';
import { LoginData, AuthData, JwtData, ConfirmData } from '../../models/_index';
import { AuthService, ErrorHelper, LoaderService, RegistrationService } from '@app/_services/';
import { LocalStorageService } from 'angular-2-local-storage';
import { Router } from '@angular/router';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { RegistrationComponent } from '@app/components/registration/registration.component';
import { ConfirmComponent } from '@app/_directives';

@Component({
    selector: 'app-phoneconfirm',
    templateUrl: './phoneconfirm.component.html',
    styleUrls: ['./phoneconfirm.component.scss']
})
export class PhoneConfirmComponent implements OnInit {

    private userName: string;
    private confirmModel: ConfirmData;
    private error = '';

    constructor(
        private router: Router,
        private errorHelper: ErrorHelper,
        private modalService: BsModalService,
        private loader: LoaderService,
        private registrationService: RegistrationService,
        public bsModalRef: BsModalRef
    ) { }

    ngOnInit() {
        this.confirmModel = new ConfirmData()
        this.confirmModel.username = this.userName.replace('+', '');
    }

    private confirm() {
        this.loader.show();

        this.registrationService.confirmPhone(this.confirmModel)
            .then((data) => {
                this.loader.hide();
                const initialState = { success: 'Phone is confirmed' };
                this.bsModalRef.hide();
                this.modalService.show(ConfirmComponent, { initialState });
            }).catch((error) => {
                this.error = this.errorHelper.parseError(error);
                this.loader.hide();
            });
    }
}
