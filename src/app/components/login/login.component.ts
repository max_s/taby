import { Component, OnInit } from '@angular/core';
import { LoginData, AuthData, JwtData, ConfirmData } from '../../models/_index';
import { AuthService, ErrorHelper, LoaderService, RegistrationService } from '@app/_services/';
import { LocalStorageService } from 'angular-2-local-storage';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { RegistrationComponent } from '@app/components/registration/registration.component';
import { ConfirmComponent } from '@app/_directives';
import { PhoneConfirmComponent } from '@app/components/phoneconfirm/phoneconfirm.component';
import { RestorePasswordComponent } from '@app/components/restorepassword/restorepassword.component';
import { Observable } from 'rxjs/Observable';
import { Subscription } from 'rxjs/Subscription';
import { environment } from './../../../environments/environment';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  private loginModel: LoginData;
  private error = '';
  private isNotComfirm = false;
  private isForgotPassword = false;

  constructor(
    private authService: AuthService,
    private localStorageService: LocalStorageService,
    private router: Router,
    private errorHelper: ErrorHelper,
    private modalService: BsModalService,
    private loader: LoaderService,
    private bsModalRef: BsModalRef,
    private registrationService: RegistrationService,
  ) { }

  ngOnInit() {
    this.loginModel = new LoginData();
    this.init();
  }

  private init() {
    this.error = '';
    this.isNotComfirm = false;
    this.isForgotPassword = false;
  }

  private login() {
    this.loader.show();
    this.init();
    this.authService.signIn(this.loginModel)
      .then((data) => {
        let authData = JSON.parse(data._body) as AuthData;
        this.loader.hide();
        if (authData.message) {
          this.error = authData.message;
          this.isNotComfirm = authData.errorcode == 1 || authData.errorcode == 2;
          this.isForgotPassword = authData.errorcode == 3;
        }
        else {
          let jwtData = JSON.parse(authData.data) as JwtData;
          this.localStorageService.set('currentUser', jwtData);
          this.bsModalRef.hide();
          this.router.navigate(['/']);
        }
      }).catch((error) => {
        this.error = this.errorHelper.parseError(error);
        this.loader.hide();
      });
  }

  private signup() {
    this.bsModalRef.hide();
    this.modalService.show(RegistrationComponent);
  }

  private sendconfirmcode() {
    this.loader.show();
    this.init();

    let confirmModel = new ConfirmData();
    confirmModel.username = this.loginModel.username;
    this.registrationService.sendConfirmationCode(confirmModel)
      .then((data) => {
        this.loader.hide();
        this.bsModalRef.hide();
        if (this.loginModel.username.indexOf('@') > -1) {
          const initialState = { success: 'Confirm code was sent. Please check your email address.' };
          this.modalService.show(ConfirmComponent, { initialState });
        }
        else {
          const initialState = { userName: this.loginModel.username };
          this.modalService.show(PhoneConfirmComponent, { initialState });
        }
      }).catch((error) => {
        this.error = this.errorHelper.parseError(error);
        this.loader.hide();
      });
  }

  private forgotpassword() {
    this.bsModalRef.hide();
    const initialState = { userName: this.loginModel.username };
    this.modalService.show(RestorePasswordComponent, { initialState });
  }
}
