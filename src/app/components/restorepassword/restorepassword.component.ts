import { Component, OnInit } from '@angular/core';
import { LoginData, AuthData, JwtData, ConfirmData } from '../../models/_index';
import { AuthService, ErrorHelper, LoaderService, RegistrationService } from '@app/_services/';
import { LocalStorageService } from 'angular-2-local-storage';
import { Router } from '@angular/router';
import { BsModalService, ModalOptions, BsModalRef } from 'ngx-bootstrap/modal';
import { RegistrationComponent } from '@app/components/registration/registration.component';
import { ConfirmComponent } from '@app/_directives';
import { ResetPasswordComponent } from '@app/components/resetpassword/resetpassword.component';

@Component({
    selector: 'app-restorepassword',
    templateUrl: './restorepassword.component.html',
    styleUrls: ['./restorepassword.component.scss']
})
export class RestorePasswordComponent implements OnInit {

    private userName: string;
    private confirmModel: ConfirmData;
    private error = '';

    constructor(
        private router: Router,
        private errorHelper: ErrorHelper,
        private modalService: BsModalService,
        private loader: LoaderService,
        private registrationService: RegistrationService,
        public bsModalRef: BsModalRef
    ) { }

    ngOnInit() {
        this.confirmModel = new ConfirmData()
        this.confirmModel.username = this.userName;
    }

    private restore() {
        this.loader.show();

        this.registrationService.restorePassword(this.confirmModel)
            .then((data) => {
                this.loader.hide();
                this.bsModalRef.hide();
                if (this.confirmModel.username.indexOf('@') > -1) {
                    const initialState = { success: 'Please check email and reset your password' };
                    this.modalService.show(ConfirmComponent, { initialState });
                }
                else {
                    const initialState = { userName: this.confirmModel.username, isPhone: true };
                    this.modalService.show(ResetPasswordComponent, { initialState });
                }
            }).catch((error) => {
                this.error = this.errorHelper.parseError(error);
                this.loader.hide();
            });
    }
}
