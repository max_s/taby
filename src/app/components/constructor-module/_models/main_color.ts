export class Main_color {
  id: string;
  color_eng: string;
  color_rus: string;
  color_let: string;
  color: string;
  tabby_color: string;
}
