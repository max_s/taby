import {
  Directive, Input, OnInit,
  ElementRef, Renderer2,
  OnChanges, SimpleChange, SimpleChanges } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '@env/environment';

@Directive({
  selector: '[appLoadSvg]'
})
export class LoadSvgDirective implements OnInit, OnChanges {
  @Input('appLoadSvg') svg_url;
  @Input('color') color;
  @Input('gradient') gradient;
  @Input() path;
  elem: HTMLElement;
  headers = {
    headers: new HttpHeaders({
      'Content-Type': 'text/xml'
    }),
    responseType: 'text' as 'text'
  };

  constructor(
    private renderer: Renderer2,
    private elRef: ElementRef,
    private http: HttpClient,
  ) {
    this.elem = this.elRef.nativeElement;
  }

  ngOnInit() {
    if (this.svg_url) {
      this.load_svg(this.svg_url, this.path, () => {
        if (this.gradient) {
          return this.set_gradient(this.color);
        }
        if (this.color) this.set_color(this.color);
      });
    }
  }

  ngOnChanges(data: SimpleChanges){
    let c_param = this.check_params(data);
    if (c_param('svg_url')) {
      if (!this.svg_url) {
        return this.set_visible('none');
      }
      this.set_visible('block');
      this.load_svg(data.svg_url.currentValue, this.path, () => this.gradient ? this.set_gradient(this.color) : this.set_color(this.color) );
    }

    if (c_param('color') && this.svg_url && !this.gradient) {
       this.set_color(data.color.currentValue);
    }

    if (this.gradient) this.set_gradient(this.color);

    if (c_param('path')) {
      this.load_svg(this.svg_url, data.path.currentValue, () => this.gradient ? this.set_gradient(this.color) : this.set_color(this.color));
    }

    // if (data.color && !data.color.firstChange) {
    //   this.set_color(data.color.currentValue);
    // }
    // if (!data.svg_url.firstChange) {
    //   if (!data.svg_url.currentValue){
    //     return this.set_visible('none');
    //   }
    //   this.set_visible('block');
    //   this.load_svg(data.svg_url.currentValue, 'tabby_patterns/')
    // }
  }

  check_params = param => (value: string) => param[value] && !param[value].firstChange;

  insert_svg(svg) {
    this.elem.innerHTML = svg;
  }

  set_visible(flag: 'none'| 'block') {
    this.renderer.setStyle(this.elem, 'display', flag);
  }

  set_gradient(color: string) {
    const _elms = Array.from(this.elem.querySelectorAll('[id^=gr_id] stop'));
    if (_elms.length) _elms.forEach( el => this.renderer.setStyle(el, 'stop-color', color));
  }

  set_color(color: string) {
    const _elms = Array.from(this.elem.querySelectorAll('.fil0'));
    if (_elms.length) _elms.forEach( el => this.renderer.setStyle(el, 'fill', color));
  }

  load_svg(svg, path: string = 'global/', cb?: Function){
    let _path = environment.host+'./assets/svg_contour/'+ path;
    return this.http.get(`${_path}${svg}.svg`, this.headers).subscribe(
      res => {
        this.insert_svg(res);
        if (cb) cb();
      },
      err => console.log(err)
    )
  }
}
