import { Component, OnInit, Input, DoCheck } from '@angular/core';

@Component({
  selector: 'app-svg-items',
  templateUrl: './svg-items.component.html',
  styleUrls: ['./svg-items.component.scss']
})
export class SvgItemsComponent implements OnInit, DoCheck {
  @Input('options') options: any;
  previous_state: any = {};
  colors: any = {};
  constructor() { }

  ngOnInit() {
  }

  ngDoCheck(){
    if (this.options.hasOwnProperty('silver_gold_color')) {
      this.set_colors(this.options);
      if (this.options.silver_gold_color){
        this.set_colors('tabby_color', this.options.silver_gold_color);
      }
    }
  }

  set_colors (data, value?: string) {

    if (value) return this.colors[data] = value;

    this.colors = {
      main_color: data.color.color,
      tabby_color: data.color.tabby_color,
      silver_gold_color: data.silver_gold_color,
      pnef: data.color.pnef,
      tcsp: data.color.tcsp,
      tortie: data.color.tortie,
      tortie_pattern: data.color.tortie_pattern
    };
  }

}
