import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SvgItemsComponent } from './svg-items.component';

describe('SvgItemsComponent', () => {
  let component: SvgItemsComponent;
  let fixture: ComponentFixture<SvgItemsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SvgItemsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SvgItemsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
