import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import {ConstructorService} from "./_services";
import {FilterColorPipe, FilterSilverPipe} from "./_pipes";
import {SvgItemsComponent} from "./svg-items/svg-items.component";
import {ConstructorComponent} from "./constructor.component";
import { LoadSvgDirective } from './_directives/load-svg.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule
  ],
  declarations: [
    ConstructorComponent,
    SvgItemsComponent,
    LoadSvgDirective,
    FilterColorPipe,
    FilterSilverPipe
  ],
  exports: [
    ConstructorComponent
  ],
  providers: [
    ConstructorService,
  ]
})
export class ConstructorModule { }
