import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable()

export class ConstructorService {

  private data_path = environment.host+'/assets/mock_data/';

  headers = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    }),
    responseType: 'json' as 'json'
  };

  constructor(
    private http: HttpClient
  ) { }

  get_main_color = () => this._http('get', this.data_path+'main_color.json');

  get_eyes = () => this._http('get', this.data_path+'eyes.json');

  get_tabby_patterns = () => this._http('get', this.data_path+'tabby_patterns.json');

  get_silver = () => this._http('get', this.data_path+'silver_gold.json');

  get_point_color = () => this._http('get', this.data_path+'point_color.json');

  get_whitespot = () => this._http('get', this.data_path+'whitespot.json');

  get_odd_eyes = () => this._http('get', this.data_path+'odd_eyes.json');

  get_breed = () => this._http('get', this.data_path+'breeds.json');

  _http = (method: string, _url: string, headers?: any ) => this.http[method](_url, headers || this.headers);

}
