import { Component, OnInit } from '@angular/core';
import { LoginData, AuthData, RegistrationData, ConfirmData } from '../../models/_index';
import { AuthService, ErrorHelper, RegistrationService, LoaderService } from '@app/_services/';
import { LocalStorageService } from 'angular-2-local-storage';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import * as moment from 'moment';
import { ConfirmComponent } from '@app/_directives';
import { PhoneConfirmComponent } from '@app/components/phoneconfirm/phoneconfirm.component';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})

export class RegistrationComponent implements OnInit {
  private registrationModel: RegistrationData;
  private error = '';
  private files: File[];

  constructor(
    private router: Router,
    private errorHelper: ErrorHelper,
    private modalService: BsModalService,
    private bsModalRef: BsModalRef,
    private registrationService: RegistrationService,
    private loader: LoaderService,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.registrationModel = new RegistrationData();
  }

  onFileChange(event: any) {
    this.files = event.srcElement.files;

    let target = event.target,
      file = target.files[0],
      reader = new FileReader();

    reader.onload = function (e) {
      target.parentElement.querySelector('.prev_ava').setAttribute('src', e.target['result']);
    };

    reader.readAsDataURL(file);
  }

  private registration() {
    if (this.registrationModel.passwordConfirmation != this.registrationModel.password) {
      this.error = 'Password and Confirm password is not match';
      return;
    }

    let sDate = `${this.registrationModel.year}-${this.registrationModel.month}-${this.registrationModel.day}`
    let date = moment(sDate);
    if (!date.isValid()) {
      this.error = 'Birth date is not valid';
      return;
    }

    this.registrationModel.birthDate = date.toDate();
    this.registrationModel.emailOrPhoneNumberConfirmation = this.registrationModel.emailOrPhoneNumber;

    this.loader.show();
    this.registrationService.registration(this.registrationModel, this.files)
      .then((data) => {
        this.loader.hide();
        this.bsModalRef.hide();
        if (this.registrationModel.emailOrPhoneNumber.indexOf('@') > -1) {
          const initialState = { success: 'Register success. Please check your email address.' };
          this.modalService.show(ConfirmComponent, { initialState });
        }
        else {
          const initialState = { userName: this.registrationModel.emailOrPhoneNumber };
          this.modalService.show(PhoneConfirmComponent, { initialState });
        }
      }).catch((error) => {
        this.error = this.errorHelper.parseError(error);
        this.loader.hide();
      });
  }
}
