import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';


import { HomeComponent, EmailCallBackComponent, HelpComponent, SupportComponent, FaqComponent, AboutComponent, OauthComponent, PetProfileComponent } from '@app/components';

import {AuthGuard} from "@app/_services";


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'login', component: HomeComponent, data: { 'login': true } },
  { path: 'signup', component: HomeComponent, data: { 'signup': true } },
  { path: 'registration', component: EmailCallBackComponent },
  { path: 'resetpassword', component: EmailCallBackComponent },
  { path: 'home', component: HomeComponent },
  { path: 'help', component: HelpComponent },
  { path: 'support', component: SupportComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'about', component: AboutComponent },
  { path: 'oauth/vk', component: OauthComponent },
  { path: 'oauth/fb', component: OauthComponent },
  { path: 'oauth/ok', component: OauthComponent },
  { path: 'oauth/in', component: OauthComponent },
  { path: 'pet', component: PetProfileComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {}
