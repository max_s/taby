export class ResetPasswordData {
    userName: string;
    password: string;
    passwordConfirmation: string;
    resetToken: string;
}