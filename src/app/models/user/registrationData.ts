export class RegistrationData {
    emailOrPhoneNumber: string;
    emailOrPhoneNumberConfirmation: string;
    password: string;
    passwordConfirmation: string;
    birthDate: Date;
    gender: number;
    firstName: string;
    lastName: string;
    day: number;
    month: string;
    year: number;
}