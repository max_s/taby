export class AuthData {
    data: string;
    userName: string;
    message: string;
    errorcode: number;
    avatar: string;
}