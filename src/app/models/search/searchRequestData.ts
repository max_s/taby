import { SearchTraitData } from "@app/models/search/searchTraitData";

export class SearchRequestData{
    searchTerm : string;
    sex: number;
    age: number | null;
    breedId: number | null;
    isForSale : boolean | null;
    isCastrate: boolean | null;
    isShowClass: boolean | null;
    isBreeding: boolean | null;
    colorMainId: number | null;
    colorEyeId: number | null;
    colorTabbyPattern: number | null;
    traits: Array<SearchTraitData>;
}