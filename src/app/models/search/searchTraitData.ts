import { TraitData } from "@app/models/_index";

export class SearchTraitData{
    
    trait: TraitData;
    value: number | null;     

}