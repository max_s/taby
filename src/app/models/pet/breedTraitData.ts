import { TraitData } from '../_index';

export class BreedTraitData {
    trait: TraitData;
    value: number
}
