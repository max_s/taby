export class PetCardData {
    id: number;
    name: string;
    sex: string;
    age: string;
    breed: string;
    cattery: string;
    price: number;
    color: string;
    description: string;
    colorCode: string;
    profileImageUrl: string;
    father: PetCardData;
    mother: PetCardData;
}