export class BreedData {
    id: number;
    name: string;
    description: string;
    firstRecognitionYear: number;
    height: number;
    weightFrom: number;
    weightTo: number;
    lifeSpanFrom: number;
    lifeSpanTo: number;
    fennologicalSystemNames: string[];
}