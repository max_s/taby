export class AddressData {
    countryCode: string;
    state: string;
    city: string;
    zip: string;
    addressLine1: string;
    addressLine2: string;
}