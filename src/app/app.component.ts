import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Masonry, MasonryGridItem } from 'ng-masonry-grid';
import { NgMasonryGridModule } from 'ng-masonry-grid';
import { BsModalService } from 'ngx-bootstrap/modal';
import { LocalStorageService } from 'angular-2-local-storage';

import 'rxjs/add/operator/filter';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '@app/_services';
import { RegistrationComponent, LoginComponent, PetsCardComponent } from '@app/components';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './styles/header.scss',
    './styles/navigation_mobile.scss',
    './styles/footer.scss',
    './app.component.scss']
})

export class AppComponent implements OnInit, OnDestroy {
  title = 'app';
  subscribes: Subscription;

  constructor(
    private authService: AuthService,
    private modalService: BsModalService,
    private route: Router,
    private localStorageService: LocalStorageService
  ) { }

  ngOnInit() {
    this.subscribes = this.route.events.filter(event => event instanceof NavigationEnd).subscribe((e: NavigationEnd) => {
      if (/login$/.test(e.url)) {
        this.signIn();
      }
      if (/signup$/.test(e.url)) {
        this.signUp();
      }
    });
  }

  ngOnDestroy() {
    this.subscribes.unsubscribe();
  }

  signIn() {
    this.modalService.show(LoginComponent);
  }

  signUp() {
    this.modalService.show(RegistrationComponent);
  }
}
