import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { LocalStorageModule } from 'angular-2-local-storage';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ConstructorModule } from './components/constructor-module/constructor.module';

import { RequestService } from '@app/_services/common/request.service';
import { AuthService, RegistrationService, ErrorHelper, LoaderService, PetService, SearchService, BreedService, ConstructorService, FenotypeService } from '@app/_services';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './components/navmenu/_index';

import { NgMasonryGridModule } from 'ng-masonry-grid';
import { InfiniteScrollModule } from "ngx-infinite-scroll";

import { FaqComponent, AboutComponent, SupportComponent, HelpComponent, RegistrationComponent, EmailCallBackComponent, LoginComponent, PhoneConfirmComponent, RestorePasswordComponent, ResetPasswordComponent, PetsCardComponent, OauthComponent, BreedDescriptionComponent, HomeComponent, AdvanceSearchComponent, BreedSliderComponent, PetProfileComponent, ConstructorPropertiesComponent } from '@app/components';


import { LoaderComponent, ConfirmComponent, SearchFormComponent, OAuthLinkComponent, PropertyTableComponent, PawDirective, PropertyTableRowComponent, TSelectComponent, TCheckboxComponent } from '@app/_directives';
import { FilterSilverPipe, FilterColorPipe } from '@app/_pipes';

import { AuthGuard } from '@app/_services/common/auth.guard';
import { AppRoutingModule } from './app.routing.module';

@NgModule({
  declarations: [
    FaqComponent, AboutComponent, SupportComponent, HelpComponent, RegistrationComponent, EmailCallBackComponent, LoginComponent,  PhoneConfirmComponent, RestorePasswordComponent, ResetPasswordComponent, PetsCardComponent, OauthComponent, BreedDescriptionComponent, HomeComponent, AdvanceSearchComponent, BreedSliderComponent, PetProfileComponent,
// fixme FROM ONE PLACE

    AppComponent,
    NavMenuComponent,
    LoaderComponent,
    ConfirmComponent,
    SearchFormComponent,
    OAuthLinkComponent,
    PropertyTableComponent,
    PawDirective,
    PropertyTableRowComponent,
    ConstructorPropertiesComponent,
    FilterSilverPipe,
    FilterColorPipe,
    TSelectComponent,
    TCheckboxComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,

    NgMasonryGridModule,
    InfiniteScrollModule,
    FormsModule,
    ReactiveFormsModule,
    ModalModule.forRoot(),
    AppRoutingModule,
    ConstructorModule,
    LocalStorageModule.withConfig({
      prefix: 'tabby-app',
      storageType: 'localStorage'
    }),
  ],
  entryComponents: [
    LoginComponent,
    RegistrationComponent,
    ConfirmComponent,
    PhoneConfirmComponent,
    RestorePasswordComponent,
    ResetPasswordComponent,
    PetsCardComponent
  ],
  providers: [
    RequestService,
    AuthService,
    RegistrationService,
    ErrorHelper,
    AuthGuard,
    LoaderService,
    PetService,
    SearchService,
    BreedService,
    ConstructorService,
    FenotypeService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
