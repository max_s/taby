import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { AuthService } from '@app/_services/user/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(
        private router: Router,
        private authService: AuthService) { }

    canActivate() {
        if (this.authService.signedIn()) {
            return true;
        }

        this.router.navigate(['/home']);
        return false;
    }
}
