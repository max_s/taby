import { Injectable } from "@angular/core";
import { LocalStorageService } from 'angular-2-local-storage';
import { Router } from '@angular/router';

@Injectable()
export class ErrorHelper {
    constructor(
        private localStorageService: LocalStorageService,
        private router: Router) {
    }

    parseError(error: any): string {
        if (error && error.status !== 0) {
            try {
                return JSON.parse(error._body).error;
            }
            catch (e) {
                console.log(e);

                this.localStorageService.clearAll();
                this.router.navigate(['/']);
            }
        }

        this.router.navigate(['/']);
    }

}