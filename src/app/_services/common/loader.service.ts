import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject'

@Injectable()

export class LoaderService {
  private loader = new Subject<any>();
  constructor(){}

  show() {
    this.loader.next({status: true});
  }

  hide() {
    this.loader.next({status : false});
  }

  getLoader() {
    return this.loader.asObservable();
  }
}
