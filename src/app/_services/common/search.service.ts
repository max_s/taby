import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject'
import { SearchRequestData } from '@app/models/_index';
import { RequestService } from './../common/request.service';

@Injectable()

export class SearchService{
  private _advance_search: Subject<boolean> = new Subject();
  private state = false;

  constructor(private requestService: RequestService){

  }

  get advance_search() {
    return this._advance_search;
  }

  toogle_search() {
    this.state = !this.state;
    this._advance_search.next(this.state);
  }

  perfrormSearch(searchRequest: SearchRequestData){
    return this.requestService.noAuthPost('search/public',searchRequest);
  }
}
