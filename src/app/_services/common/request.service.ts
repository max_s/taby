import { Http, RequestOptions, Request, RequestMethod, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { environment } from './../../../environments/environment';
import { LocalStorageService } from 'angular-2-local-storage';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import { AuthData, JwtData } from '../../models/_index';
import * as moment from 'moment';

@Injectable()
export class RequestService {

    constructor(private http: Http, private localStorageService: LocalStorageService) {

    }

    public noAuthPost(url: string, body: any): Promise<any> {
        let options = this.getRequestOptions(url, false, true, null);
        options.body = body;
        options.method = RequestMethod.Post;

        return this.http.request(new Request(options), body)
            .map((response) => {
                return response;
            })
            .toPromise();
    }

    public noAuthPostWithToken(url: string, body: any, token: string): Promise<any> {
        let options = this.getRequestOptions(url, false, true, token);
        options.body = body;
        options.method = RequestMethod.Post;

        return this.http.request(new Request(options), body)
            .map((response) => {
                return response;
            })
            .toPromise();
    }

    public noAuthPut(url: string, body: any): Promise<any> {
        let options = this.getRequestOptions(url, false, true, null);
        options.body = body;
        options.method = RequestMethod.Put;

        return this.http.request(new Request(options), body)
            .map((response) => {
                return response;
            })
            .toPromise();
    }

    public noAuthGet(url: string): Promise<any> {
        let options = this.getRequestOptions(url, false, true, null);
        options.method = RequestMethod.Get;

        return this.http.request(new Request(options))
            .map((response) => {
                return response;
            })
            .toPromise();
    }

    public noAuthPostWithFiles(url: string, data: any, files: File[]): Promise<any> {
        let options = this.getRequestOptions(url, false, false, null);
        options.method = RequestMethod.Post;

        let body = this.setFormData(data, files);
        return this.http.post(options.url, body, new Request(options))
            .map((response) => {
                return response;
            })
            .toPromise();
    }

    public authPost(url: string, body: any): Promise<any> {
        let options = this.getRequestOptions(url, true, true, null);
        options.body = body;
        options.method = RequestMethod.Post;

        return this.http.request(new Request(options), body)
            .map((response) => {
                return response;
            })
            .toPromise();
    }

    public authPut(url: string, body: any): Promise<any> {
        let options = this.getRequestOptions(url, true, true, null);
        options.body = body;
        options.method = RequestMethod.Put;

        return this.http.request(new Request(options), body)
            .map((response) => {
                return response;
            })
            .toPromise();
    }

    public authGet(url: string): Promise<any> {
        let options = this.getRequestOptions(url, true, true, null);
        options.method = RequestMethod.Get;

        return this.http.request(new Request(options))
            .map((response) => {
                return response;
            })
            .toPromise();
    }

    public authPostWithFiles(url: string, data: any, files: File[]): Promise<any> {
        let options = this.getRequestOptions(url, true, false, null);
        options.method = RequestMethod.Post;

        let body = this.setFormData(data, files);
        return this.http.post(options.url, body, new Request(options))
            .map((response) => {
                return response;
            })
            .toPromise();
    }

    private getRequestOptions(url: string, isAuth: boolean, isJson: boolean, token: string) {
        let headers = new Headers();
        if (isAuth) {
            let authData = this.localStorageService.get('currentUser') as JwtData;
            if (authData != null) {
                headers.append('Authorization', `Bearer ${authData.access_token}`);
            }
        }
        else {
            if (token) {
                headers.append('Authorization', `Bearer ${token}`);
            }
        }
        if (isJson) {
            headers.append('Content-Type', 'application/json');
        }
        headers.append('Accept', 'application/json');
        //headers.append('Locale', localStorage.getItem('localeId'));

        if (url[0] === '/') {
            url = url.substr(1, url.length);
        }
        if (environment.apiRoot[environment.apiRoot.length - 1] === '/') {
            url = environment.apiRoot + url;
        } else {
            url = environment.apiRoot + '/' + url;
        }

        return new RequestOptions({
            url: url,
            headers: headers
        });
    }

    private setFormData(data: any, files: File[]): FormData {
        let formData: FormData = new FormData();
        if (files) {
            for (let i = 0; i < files.length; i++) {
                formData.append('file', files[i], files[i].name);
            }
        }

        formData = this.populateFormData(formData, data);

        return formData;
    }

    private populateFormData(formData: FormData, data: any, prefix: string = null) {
        for (var property in data) {
            if (data.hasOwnProperty(property) && data[property] != null) {
                if (typeof (data[property]) == "object" && property.toLowerCase().indexOf('date') == -1) {
                    formData = this.populateFormData(formData, data[property], property);
                }
                else {
                    let propertyName = `${prefix ? prefix + '.' : ''}${property}`;
                    if (property.toLowerCase().indexOf('date') > -1) {
                        formData.append(propertyName, moment(data[property]).toJSON());
                    }
                    else {
                        formData.append(propertyName, data[property]);
                    }
                }
            }
        }

        return formData;
    }
}