import { Injectable } from '@angular/core';
import { RequestService } from './../common/request.service';
import { RegistrationData, ConfirmData, ResetPasswordData } from '../../models/_index';


@Injectable()
export class RegistrationService {
    private _registrationApiUrl = 'registrations';
    private _confirmEmailApiUrl = 'registrations/email';
    private _confirmPhoneApiUrl = 'registrations/phone';
    private _codeApiUrl = 'registrations/sendcode';
    private _restorePasswordUrl = 'auth/passwordrestorerequests';
    private _resetPasswordUrl = 'auth/passwordresetrequests';

    constructor(
        private requestService: RequestService) {
    }

    registration(model: RegistrationData, files: File[]) {
        return this.requestService.noAuthPostWithFiles(this._registrationApiUrl, model, files);
    }

    confirmEmail(model: ConfirmData) {
        return this.requestService.noAuthPost(this._confirmEmailApiUrl, model);
    }

    sendConfirmationCode(model: ConfirmData) {
        return this.requestService.noAuthPost(this._codeApiUrl, model)
    }

    confirmPhone(model: ConfirmData) {
        return this.requestService.noAuthPost(this._confirmPhoneApiUrl, model);
    }

    restorePassword(model: ConfirmData) {
        return this.requestService.noAuthPost(this._restorePasswordUrl, model);
    }

    resetPassword(model: ResetPasswordData) {
        return this.requestService.noAuthPost(this._resetPasswordUrl, model);
    }
}