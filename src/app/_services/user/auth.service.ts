import { Injectable } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';
import { AuthData, LoginData, JwtData } from '../../models/_index';
import { JwtHelperService } from '@auth0/angular-jwt';
import { RequestService } from './../common/request.service';

@Injectable()
export class AuthService {
    private helper = new JwtHelperService();
    private _signoutUrl = 'auth/signout';
    private _getTokenApiUrl = 'auth/token';
    private _socialApiUrl = 'auth/social';

    constructor(
        private localStorageService: LocalStorageService,
        private requestService: RequestService) {
    }

    signIn(data: LoginData): Promise<any> {
        let body = JSON.stringify({ grant_type: 'password', client_id: data.username, username: data.username, password: data.password });

        return this.requestService.noAuthPost(this._getTokenApiUrl, body);
    }

    signIn_oauth(provider: string, code: string, redirect_url: string, token: string): Promise<any> {
        let body = JSON.stringify({ provider: provider, code: code, client_id: code, redirect_url: redirect_url });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        if (token) {
            headers.append('Authorization', `Bearer ${token}`);
            return this.requestService.noAuthPostWithToken(this._socialApiUrl, body, token);
        }
        else {
            return this.requestService.noAuthPost(this._socialApiUrl, body);
        }
    }

    signOut(): Promise<any> {
        return this.requestService.authGet(this._signoutUrl);
    }

    signedIn() {
        let authData = this.localStorageService.get('currentUser') as JwtData;
        if (authData == null)
            return false;
        let isTokenExpired = this.helper.isTokenExpired(authData.access_token);
        return !isTokenExpired;
    }

    getUserFullName() {
        let authData = this.localStorageService.get('currentUser') as AuthData;
        if (authData == null)
            return '';
        return authData.userName;
    }

    getUserAvatar() {
        let authData = this.localStorageService.get('currentUser') as AuthData;
        if (authData == null)
            return '';
        return authData.avatar;
    }
}