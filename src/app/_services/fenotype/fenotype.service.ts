import { Injectable } from '@angular/core';
import { RequestService } from './../common/request.service';


@Injectable()
export class FenotypeService {
    constructor(
        private requestService: RequestService) {
    }    

    getMainColors(){
        return this.requestService.noAuthGet(`maincolors`);
    }

    getEyeColors(){
        return this.requestService.noAuthGet(`coloreyes`);
    }

    getColorWhiteAmmounts(){
        return this.requestService.noAuthGet(`colorwhiteamounts`);
    }

    getColorPointedPatterns(){
        return this.requestService.noAuthGet(`colorpointedpatterns`);
    }

    getColorTabbyPatterns(){
        return this.requestService.noAuthGet(`colortabbypatterns`);
    }

    getColorModes(){
        return this.requestService.noAuthGet(`colormodes`);
    }
}