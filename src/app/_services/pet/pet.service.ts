import { Injectable } from '@angular/core';
import { RequestService } from './../common/request.service';


@Injectable()
export class PetService {
    constructor(
        private requestService: RequestService) {
    }    

    getPetsForLanding(page: number, pageSize: number){
        return this.requestService.noAuthGet(`pets/landing?page=${page}&pageSize=${pageSize}`);
    }

    getPet(petId:number){
        return this.requestService.noAuthGet(`pets/${petId}`);
    }
}