import { Injectable } from '@angular/core';
import { RequestService } from './../common/request.service';


@Injectable()
export class BreedService {
    private _breedApiUrl = 'breeds';
    private _traitApiUrl = 'traits';

    constructor(
        private requestService: RequestService) {
    }

    getBreeds() {
        return this.requestService.noAuthGet(this._breedApiUrl);
    }

    getBreedsById(id: number) {
        let url = `${this._breedApiUrl}/${id}`;
        return this.requestService.noAuthGet(url);
    }

    getTraits() {
        return this.requestService.noAuthGet(this._traitApiUrl);
    }

    getBreedTraits(id: number) {
        let url = `${this._breedApiUrl}/${id}/traits`;
        return this.requestService.noAuthGet(url);
    }
}