import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'filterSilver'})

export class FilterSilverPipe implements PipeTransform {
  transform(value: any[], smoke: boolean) {
    let data = value;
    if(smoke) data = value.filter( el =>  el.value !== null );
    return data;
  }
}
