import { Pipe, PipeTransform } from '@angular/core';

let exclude, exception;

@Pipe({name: 'filterPatterns'})

export class FilterColorPipe implements PipeTransform {
  shaded_off = el => {
    if(!exclude.some( value =>  value === el.svg)) {
      return el;
    }
  };
  shaded = el => {
    if([...exception, ...exclude].some( value => value === el.svg)){
      return el;
    }
  };
  transform(value: any[], smoke: boolean = false, point: boolean = false, param: {exclude: any[], exception: any[]}) {
    exception = param.exception;
    exclude = param.exclude;
    let data = value,
        filter_fn = point ? this.shaded : this.shaded_off;
    if(smoke || point) data = value.filter(filter_fn);
    return data;
  }
}
