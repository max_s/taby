$(function(){
    new Swiper('.swiper-container', {
        nextButton: '.category__arr--right',
        prevButton: '.category__arr--left',
        slidesPerView: 12,
        centeredSlides: true,
        loop: true,
        breakpoints: {
            320: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            480: {
                slidesPerView: 4,
                spaceBetween: 20
            },
            640: {
                slidesPerView: 6,
                spaceBetween: 30
            },
            768: {
                slidesPerView: 7,
                spaceBetween: 30
            },
            1054: {
                slidesPerView: 9,
                spaceBetween: 30
            }
        }
    });

});
