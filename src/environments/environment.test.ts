import { Environment } from './environment.interface';

export const environment: Environment = {
  production: true,
  apiRoot: 'http://apitest.tabbybook.com/api/v1',
  host_url: 'https://webtest.tabbybook.com',
  client_id_fb: '169491043768625',
  client_id_vk: '6127780',
  client_id_ok: '1252339456',
  client_id_in: 'e525bded2b5b487aa3d44b485e3bb02c',
  apiHost: 'http://apitest.tabbybook.com/',
  host: '.'
}
