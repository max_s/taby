import { Environment } from './environment.interface';

export const environment: Environment = {
  production: true,
  apiRoot: 'http://apitest.tabbybook.com/api/v1',
  host_url: 'https://tabbybook.com',
  client_id_fb: '1779750075648959',
  client_id_vk: '6127780',
  client_id_ok: '1252338432',
  client_id_in: '0bd2247f7a874c15ac1e7261e2df38a8',
  apiHost: 'http://apitest.tabbybook.com/',
  host: '.'
}
