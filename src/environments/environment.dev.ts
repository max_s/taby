import { Environment } from './environment.interface';

export const environment: Environment = {
  production: false,
//  apiRoot: 'http://localhost:9992/api/v1',
  apiRoot: 'http://apitest.tabbybook.com/api/v1',
  host_url: 'https://dev.tabbybook.com',
  client_id_fb: '208383803095146',
  client_id_vk: '6127227',
  client_id_ok: '1266202368',
  client_id_in: '2e69c1c112434bcfa17573475b9e3dd6',
  apiHost:'http://apitest.tabbybook.com/',
  host: '.'
}
