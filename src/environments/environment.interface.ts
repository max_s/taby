export interface Environment {
    apiRoot: string,
    production: boolean,
    host_url: string;
    client_id_vk: string,
    client_id_fb: string,
    client_id_ok: string,
    client_id_in: string,
    apiHost: string,
    host: string
}
